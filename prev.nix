{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
          haskell-env = pkgs.haskellPackages.ghcWithHoogle( hp: with hp; [
            brick
            cursor
          ]);
          miscPkgs =  with pkgs;
            [ 
              ghcid
              haskell-language-server
              hlint
            ];
      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            haskell-env
          ] ++ miscPkgs ;
            };
      });
}

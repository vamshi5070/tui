{-# LANGUAGE OverloadedStrings #-}

module Tui where

import Brick.AttrMap
import Brick.Main
import Brick.Types
import Brick.Util
import Brick.Widgets.Border
import Brick.Widgets.Center
import Brick.Widgets.Core
import Brick.Widgets.Skylighting (attrMappingsForStyle, highlight)
import Control.Monad
import Control.Monad.IO.Class
import Cursor.Brick.TextField
import Cursor.TextField
import Cursor.Types
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text.IO as T
import Graphics.Vty.Attributes
import Graphics.Vty.Input.Events
import Path
import Path.IO
import qualified Skylighting.Core as S
import Skylighting.Syntax
import System.Directory
import System.Environment
import System.Exit
import Text.Show.Pretty

tui :: IO ()
tui = do
  args <- getArgs
  case args of
    [] -> die "No argument to choose file to edit."
    (fp : _) -> do
      path <- resolveFile' fp
      maybeContents <- forgivingAbsence $ T.readFile (fromAbsFile path)
      let contents = fromMaybe "" maybeContents
      initialState <- buildInitialState contents
      endState <- defaultMain tuiApp initialState
      let contents' = rebuildTextFieldCursor (stateCursor endState)
      unless (contents == contents') $ T.writeFile (fromAbsFile path) contents'

givePath = do
  args <- getArgs
  case args of
    [] -> die "No argument to choose file to edit."
    (fp : _) -> do
      resolveFile' fp

newtype TuiState = TuiState
  { stateCursor :: TextFieldCursor
  }
  deriving (Show, Eq)

data ResourceName
  = ResourceName
  deriving (Show, Eq, Ord)

-- vp1Scroll ::ViewportScroll

tuiApp :: App TuiState e ResourceName
tuiApp =
  App
    { appDraw = drawTui,
      appChooseCursor = showFirstCursor,
      appHandleEvent = handleTuiEvent,
      appStartEvent = pure,
      appAttrMap = const $ attrMap defAttr $ attrMappingsForStyle $ S.breezeDark
      --attrMap mempty [("text", fg red), ("bg", fg blue)]
    }

buildInitialState :: Text -> IO TuiState
buildInitialState contents = do
  let tfc = makeTextFieldCursor contents
  pure TuiState {stateCursor = tfc}

drawTui :: TuiState -> [Widget ResourceName]
drawTui ts =
  [ --
    --     forceAttr "text" $
    -- centerLayer $
    --       border $
    -- padLeftRight
    --         padAll 1 $ selectedTextFieldCursorWidget ResourceName (stateCursor ts),
    -- viewport ResourceName Vertical $
 highlight haskellLang $ rebuildTextFieldCursor (stateCursor ts)
    --    forceAttr "bg" $ fill '@'
  ]

vp1Scroll = viewportScroll ResourceName

-- handleTuiEvent :: TuiState -> BrickEvent n e -> EventM n (Next TuiState)
handleTuiEvent s e =
  case e of
    VtyEvent vtye ->
      let mDo ::
            (TextFieldCursor -> Maybe TextFieldCursor) ->
            EventM n (Next TuiState)
          mDo func = do
            let tfc = stateCursor s
            let tfc' = fromMaybe tfc $ func tfc
            let s' = s {stateCursor = tfc'}
            continue s'
          mDo' func = do
            let tfc = stateCursor s
            let tfc' = func tfc
            let s' = s {stateCursor = tfc'}
            continue s'
       in case vtye of
            EvKey (KChar 's') [MCtrl] -> do
              path <- liftIO $ givePath
              let contents' = rebuildTextFieldCursor (stateCursor s)
              liftIO $ T.writeFile (fromAbsFile path) contents'
              continue s
            EvKey (KChar 'n') [MCtrl] -> do
              vScrollBy vp1Scroll 1
              mDo textFieldCursorSelectNextLine
            -- continue s
            EvKey (KChar 'p') [MCtrl] -> do
              vScrollBy vp1Scroll (-1)
              mDo textFieldCursorSelectPrevLine
            -- continue s
            EvKey (KChar 'z') [MCtrl] -> halt s
            EvKey (KChar c) [] -> mDo $ textFieldCursorInsertChar c . Just
            EvKey KUp [] -> mDo textFieldCursorSelectPrevLine
            EvKey KDown [] -> mDo textFieldCursorSelectNextLine
            EvKey KRight [] -> do
              let tfc = stateCursor s
              let tfc' = fromMaybe tfc $ textFieldCursorSelectNextChar tfc
              let s' = s {stateCursor = tfc'}
              -- let tfc' =  stateCursor s
              if tfc == tfc'
                then mDo (textFieldCursorSelectNextLine . textFieldCursorSelectStartOfLine)
                else continue s'
            EvKey KLeft [] -> do
              let tfc = stateCursor s
              let tfc' = fromMaybe tfc $ textFieldCursorSelectPrevChar tfc
              let s' = s {stateCursor = tfc'}
              if tfc == tfc'
                then mDo $ (fmap textFieldCursorSelectEndOfLine . Just) <=< textFieldCursorSelectPrevLine
                else continue s'
            -- import Cursor.Types
            EvKey KBS [] -> mDo $ dullMDelete . textFieldCursorRemove
            EvKey KDel [] -> mDo $ dullMDelete . textFieldCursorDelete
            EvKey KEnter [] -> mDo $ Just . textFieldCursorInsertNewline . Just
            EvKey KEsc [] -> halt s
            -- EvKey MAlt [KChar 'z'] -> halt s
            _ -> continue s
    _ -> continue s

haskellLang = fromJust $ S.syntaxByName defaultSyntaxMap "haskell"

-- TODO: Implement Undo
-- DONE: Go to next line if at end of line and press right
-- TODO: duplicate line
-- TODO: Copy-pasting
-- TODO: replace mode
-- TODO: move to end of line
-- TODO: move to end of word
-- TODO: uppercase mode
-- TODO: uppercase current char
-- TODO: uppercase current word
vamshi = 1

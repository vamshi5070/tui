import Control.Monad

one = fmap (+1) .  Just

exp1 x = Just (x+1)
exp2 x = Just (x+2)

composeExp f g = fmap f . g


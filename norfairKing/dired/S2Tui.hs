{-# LANGUAGE OverloadedStrings #-}

module Tui where

import System.Directory

import Brick.AttrMap
import Brick.Main
import Brick.Types
import Brick.Widgets.Core
import Brick.Util

import Graphics.Vty.Input.Events
import Graphics.Vty.Attributes

import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.List.NonEmpty as NE

import Cursor.Simple.List.NonEmpty

import System.Environment
import System.Exit


tui :: IO ()
tui = do
  args <- getArgs
  initialState <- buildInitialState' args
  endState <- defaultMain tuiApp initialState
  print endState
  
--buildInitialState = do
 --b  curDir <- getCurrentDirectory
 --b  content <- getDirectoryContents curDir
 --b  return $ TuiState {tuiStatePaths = content}

buildInitialState' args = do
   curDir  <- if null args  then getCurrentDirectory else return $ head args
--   curDir <- getCurrentDirectory
   content <- getDirectoryContents curDir
   case NE.nonEmpty content of
      Nothing -> die "Invalid path" -- There are no contents"
      Just ne -> return $ TuiState {tuiStatePaths = makeNonEmptyCursor ne }

tuiApp :: App TuiState e ResourceName
tuiApp = App {
  appDraw = drawTui
  , appChooseCursor = showFirstCursor
  , appHandleEvent = handleTuiEvent
  , appStartEvent = return
  , appAttrMap = const $ attrMap mempty [("selected", fg red)]
  }

newtype TuiState= TuiState {tuiStatePaths :: NonEmptyCursor FilePath}
  deriving (Eq,Show)

type ResourceName = String
  
drawTui ts =   [vBox $ concat
  [      map (drawPath False) $ reverse $ nonEmptyCursorPrev nonMtCursors
    , [drawPath True  $ nonEmptyCursorCurrent nonMtCursors]
    , map (drawPath False ) $ nonEmptyCursorNext nonMtCursors
  ]
       ] 
  where nonMtCursors = tuiStatePaths ts
  
drawPath :: Bool -> FilePath -> Widget n
drawPath True = withAttr "selected" . str
drawPath False = str

handleTuiEvent :: TuiState -> BrickEvent n e -> EventM n (Next TuiState)
handleTuiEvent state event = case event of
  VtyEvent vtye -> case vtye of 
    EvKey (KChar 'q') [] -> halt state
    EvKey (KChar 'j') [] -> continue $ goDown state
    EvKey (KChar 'k') [] -> continue $ goUp state

goDown state = do
  let nonMtCursor = tuiStatePaths state
  case nonEmptyCursorSelectNext nonMtCursor of
    Nothing -> state
    Just nonMtCursor' -> state {tuiStatePaths = nonMtCursor'}

goUp state = do
  let nonMtCursor = tuiStatePaths state
  case nonEmptyCursorSelectPrev nonMtCursor of
    Nothing -> state
    Just nonMtCursor' -> state {tuiStatePaths = nonMtCursor'}

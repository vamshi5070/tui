{-# LANGUAGE OverloadedStrings #-}

module Tui where

import System.Directory

import Brick.AttrMap
import Brick.Main
import Brick.Types
import Brick.Widgets.Core
import Graphics.Vty.Input.Events

import System.Environment

tui :: IO ()
tui = do
  args <- getArgs
  initialState <- buildInitialState' args
  endState <- defaultMain tuiApp initialState
  print endState
  
buildInitialState = do
   curDir <- getCurrentDirectory
   content <- getDirectoryContents curDir
   return $ TuiState {tuiStatePaths = content}

buildInitialState' args = do
   curDir  <- if null args  then getCurrentDirectory else return $ head args
--   curDir <- getCurrentDirectory
   content <- getDirectoryContents curDir
   return $ TuiState {tuiStatePaths = content}

tuiApp :: App TuiState e ResourceName
tuiApp = App {
  appDraw = drawTui
  , appChooseCursor = showFirstCursor
  , appHandleEvent = handleTuiEvent
  , appStartEvent = return
  , appAttrMap = const $ attrMap mempty []
  }

newtype TuiState= TuiState {tuiStatePaths :: [FilePath]}
  deriving (Eq,Show)

type ResourceName = String
  
drawTui ts = [vBox $ map drawPath $ tuiStatePaths ts] 

handleTuiEvent :: TuiState -> BrickEvent n e -> EventM n (Next TuiState)
handleTuiEvent state event = case event of
  VtyEvent vtye -> case vtye of EvKey (KChar 'q') [] -> halt state

drawPath :: FilePath -> Widget n
drawPath = str  
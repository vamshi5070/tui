{-# LANGUAGE OverloadedStrings #-}

module MyTui where

import System.Directory

import Control.Monad.IO.Class
import Control.Monad

import Brick.AttrMap
import Brick.Main
import Brick.Types
import Brick.Widgets.Core
import Brick.Util

import Graphics.Vty.Input.Events
import Graphics.Vty.Attributes

import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.List.NonEmpty as NE

import Cursor.Simple.List.NonEmpty

import System.Environment
import System.Exit

tui :: IO ()
tui = do
  args <- getArgs
--  putStrLn $ head args
  curDir <- getCurrentDirectory
--  setCurrentDirectory curDir
  let actualDir =  if null args then curDir else head args
  b <- doesDirectoryExist actualDir
  if b then
    do
--  setCurrentDirectory actualDir
    initialState <- buildInitialState' 
    endState <- defaultMain tuiApp initialState
    print endState
  else
    error "enter a proper directory path!!"
  
--buildInitialState = do
 --b  curDir <- getCurrentDirectory
 --b  content <- getDirectoryContents curDir
 --b  return $ TuiState {tuiStatePaths = content}

buildInitialState' ::   IO TuiState
buildInitialState' = do
--   curDir <- canonicalizePath dir
   curDir <- getCurrentDirectory
--   setCurrentDirectory curDir
-- getHomeDirectory
-- if doesDirectoryExist curDir then getDirectoryContents curDir else 
   content <- getDirectoryContents curDir
   contents' <- forM content $ \fp -> do
     pred <- doesDirectoryExist fp 
     if not pred then return $ File fp
     else return $ Directory fp

   case NE.nonEmpty contents' of
      Nothing -> die "Invalid path" -- There are no contents"
      Just ne -> return $ TuiState {tuiStatePaths = makeNonEmptyCursor ne }

data POC = File FilePath | Directory FilePath deriving (Eq,Show)

tuiApp :: App TuiState e ResourceName
tuiApp = App {
  appDraw = drawTui
  , appChooseCursor = showFirstCursor
  , appHandleEvent = handleTuiEvent
  , appStartEvent = return
  , appAttrMap = const $ attrMap mempty [
    ("selected" <> "file",fg green)
    , ("selected" <> "directory" , fg magenta)
    , ("selected", fg magenta)
    ,("file", fg white)
    , ("header",fg cyan)
    ,("directory",fg yellow)]
  }

newtype TuiState= TuiState {tuiStatePaths :: NonEmptyCursor POC} --,currentDir :: FilePath}
  deriving (Eq,Show)

type ResourceName = String
--  padTop (Pad 1) (createAbsolute (nonEmptyCursorCurrent nonMtCursors)) ,
drawTui ts =   [vBox $ concat
  [  map (drawPath False) $ reverse $ nonEmptyCursorPrev nonMtCursors
    , [drawPath True  $ nonEmptyCursorCurrent nonMtCursors]
    , map (drawPath False ) $ nonEmptyCursorNext nonMtCursors
  ]
       ] 
  where nonMtCursors = tuiStatePaths ts
  
createAbsolute :: POC -> Widget n
createAbsolute (Directory fp) = withAttr "header" $ str fp
createAbsolute (File fp) = withAttr "header" $ str fp
  
drawPath :: Bool -> POC -> Widget n
drawPath pred poc = (if pred then 
   forceAttr "selected" 
      else id ) $ case poc of
        (File fp) -> withAttr "file" $ str fp
        (Directory fp) -> withAttr "directory" $ str fp
--drawPath pred (Directory fp)= if pred then withAttr "selected" $ str fp else  str fp
--drawPath pred (File fp)= if pred then withAttr "selected" $ str fp else str fp
--drawPath False = str

handleTuiEvent :: TuiState -> BrickEvent n e -> EventM n (Next TuiState)
handleTuiEvent state event = case event of
  VtyEvent vtye -> case vtye of 
    EvKey (KChar 'q') [] -> halt state
    EvKey (KChar 'h') [] -> do
      curDir <- liftIO getCurrentDirectory 
      let curDir' = curDir <> "/../"
      liftIO $ setCurrentDirectory curDir'
      state' <- liftIO  buildInitialState' 
      continue state'

    --continue $ goDown state
    EvKey (KChar 'j') [] -> continue $ goDown state
    EvKey (KChar 'k') [] -> continue $ goUp state
    EvKey (KChar 'l') [] -> do
      let fp = nonEmptyCursorCurrent $ tuiStatePaths state
      case fp of 
        File _ -> continue state
        Directory curDir -> do
          --getDirectory <- liftIO (doesDirectoryExist curDir) 
          --if getDirectory then  do
          liftIO $ setCurrentDirectory curDir
          state' <- liftIO  buildInitialState' 
          continue state'
--          else
--            continue state
--      curDirActual <- if getDirectory then return curDir else liftIO getCurrentDirectory

goDown state = do
  let nonMtCursor = tuiStatePaths state
  case nonEmptyCursorSelectNext nonMtCursor of
    Nothing -> state
    Just nonMtCursor' -> state {tuiStatePaths = nonMtCursor'}

goUp state = do
  let nonMtCursor = tuiStatePaths state
  case nonEmptyCursorSelectPrev nonMtCursor of
    Nothing -> state
    Just nonMtCursor' -> state {tuiStatePaths = nonMtCursor'}

--goInside :: TuiState -> TuiState
--goInside state = do
--  state'
----  return state'
--        goInside state
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE RankNTypes #-}

type Lens s a = forall f. Functor f => (a -> f a) -> (s -> f s)

newtype Identity a = Identity { runIdentity :: a }
  deriving Functor

newtype Const a b = Const { getConst :: a }
  deriving Functor

over :: Lens s a -> (a -> a) -> s -> s
over lens f s = runIdentity (lens (Identity . f) s)

view :: Lens s a -> s -> a
view lens s = getConst (lens Const s )

-- Composing lens
lens :: (s -> a) -> (s -> a -> s) -> Lens s a 
lens getter setter = \f s -> setter s <$> f (getter s)


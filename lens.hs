-- # LANGUAGE OverloadedStrings #

-- import Data.String(String)

data Address = Address
  { addressCity :: !String,
    addressStreet :: !String
  }

data Person = Person -- pascal
  { personAddress :: !Address,
    personName :: !String
  }

getPersonCity :: Person -> String
getPersonCity = addressCity . personAddress

setPersonCity :: String -> Person -> Person
setPersonCity city person =
  person
    { personAddress = (personAddress person) {addressCity = city}
    }

modifyAddressCity :: (String -> String) -> Address -> Address
modifyAddressCity f address =
  address
    { addressCity = f (addressCity address)
    }

modifyPersonAddress :: (Address -> Address) -> Person -> Person
modifyPersonAddress f person =
  person
    { personAddress = f (personAddress person)
    }

modifyPersonCity :: (String -> String) -> Person -> Person
modifyPersonCity = modifyPersonAddress . modifyAddressCity

setPersonCity' :: String -> Person -> Person
setPersonCity' city = modifyPersonCity (\_ -> city)

const' :: a -> b -> a
const' x _ = x

const'' :: a -> b -> a
const'' x = (\_ -> x)

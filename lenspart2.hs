{-# LANGUAGE DeriveFunctor #-}

data Address = Address
  { addressCity   :: !String,
    addressStreet :: !String
  }

data Person = Person -- pascal
  { personAddress :: !Address,
    personName    :: !String
  }

newtype Identity a = Identity {runIdentity :: a}
  deriving (Functor, Show) --(Eq,Show)

-- type LensModify s a = (s -> Identity s) -> (a -> Identity a)
type LensModify s a = (a -> Identity a) -> (s -> Identity s)

over :: LensModify s a -> (a -> a) -> s -> s
over lens f s = runIdentity (lens (Identity . f) s)

personAddressL :: LensModify Person Address                                 -- Person -> Address   s -> Identity s
personAddressL f person = modifyPersonAddress <$> f (personAddress person)
  where modifyPersonAddress address =   person
          { personAddress = address
          }
-- personAddressL f person = Identity $ person { personAddress = runIdentity $ f $ personAddress $ person}

newtype Const a b = Const { getConst :: a}
  deriving (Functor, Show) --(Eq,Show)

-- type LensGetter s a  = s ->  Const a s

-- view :: LensGetter s a -> s -> a
-- view lens s = getConst $ lens s

-- personAddressL' :: LensGetter Person Address
-- personAddressL' = Const . personAddress 

type LensGetter s a  = (a ->  Const a s) -> (s ->  Const a s)

view :: LensGetter s a -> s -> a
view lens s = getConst ( lens Const s )

personAddressL' :: LensGetter Person Address
personAddressL' f s = Const $ getConst $ f $ personAddress s

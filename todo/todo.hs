import Brick
import Brick.Widgets.List
import Brick.Widgets.Center
import Brick.Widgets.Edit
import Graphics.Vty
import Control.Monad (when)

-- Define our data types
data Name = List | Add | Edit
  deriving (Ord, Show, Eq)

data AppState = AppState
  { todoList :: List Name String
  , addBox :: Edit String
  , editBox :: Maybe (Edit String)
  }

-- Initialize the app with an empty list and an empty add box
initState :: AppState
initState = AppState
  { todoList = list List (map (\x -> (x, x)) []) 1
  , addBox = editText ""
  , editBox = Nothing
  }

-- Convert the AppState to a list of widgets
app :: App AppState e Name
app = App
  { appDraw = drawUI
  , appChooseCursor = showFirstCursor
  , appHandleEvent = handleEvent
  , appStartEvent = return
  , appAttrMap = const theMap
  }

-- Define the attribute map
theMap :: AttrMap
theMap = attrMap defAttr
  [ (editAttr, white `on` black)
  , (listSelectedAttr, black `on` white)
  ]

-- Draw the UI
drawUI :: AppState -> [Widget Name]
drawUI s =
  [ center $ vBox
    [ hBox [ drawList (todoList s)
           , vBox [ widgetAdd
                  , widgetEdit s
                  ]
           ]
    ]
  ]

-- Add a new todo
widgetAdd :: Widget Name
widgetAdd = withAttr editAttr $
            vBox [ txt "Add Todo"
                 , renderEditor (txt . unlines) True (addBox s)
                 ]

-- Edit a todo
widgetEdit :: AppState -> Widget Name
widgetEdit s = case editBox s of
  Just e -> withAttr editAttr $
            vBox [ txt "Edit Todo"
                 , renderEditor (txt . unlines) True e
                 ]
  Nothing -> str ""

-- Handle events
handleEvent :: AppState -> BrickEvent Name e -> EventM Name (Next AppState)
handleEvent s (VtyEvent e) =
  case e of
    EvKey KEnter [] -> do
      -- Add a new todo
      let newTodo = unlines $ getEditContents (addBox s)
      let newTodos = newTodo : snd (listElements (todoList s))
      let newList = list List (map (\x -> (x, x)) newTodos) (listSelectedElement (todoList s))
      continue $ s { todoList = newList, addBox = editText "" }
    EvKey KEsc [] -> do
      -- Cancel add or edit
      let eBox = editBox s
      continue $ s { editBox = Nothing }
    EvKey KDown [] -> do
      -- Move the cursor down in the list
      let newList = listMoveDown (todoList s)
      continue $ s --{


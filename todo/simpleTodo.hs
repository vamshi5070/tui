import System.IO

type Task = String

type TodoItem = (Task, Bool)

type TodoList = [TodoItem]

emptyTodoList :: TodoList
emptyTodoList = []

addTask :: TodoList -> Task -> TodoList
addTask todoList task = (task,False) : todoList

completeTask :: TodoList -> Task -> TodoList
completeTask [] _ = []
completeTask (x:xs) task
	| (fst x) == task = (task,True):xs
	| otherwise = x: completeTask xs task

removeTask :: TodoList -> Task -> TodoList
removeTask [] _ = []
removeTask (x:xs) task
	| (fst x) == task = removeTask xs task
	| otherwise = x:removeTask xs task

displayTodoList :: TodoList -> IO ()
displayTodoList [] = putStrLn "No tasks to display"
displayTodoList [(task, completed)] = 	putStrLn $ task <> if completed then " (completed) " 
						else " (incomplete) "
	
--putStrLn "No tasks to display"
displayTodoList ((task, completed):xs) = do
	putStrLn $ task <> if completed then " (completed) " 
				else " (incomplete) "
	displayTodoList xs

main = do
	putStrLn "Welcome to the Todo application"
	putStrLn "Choose an option:"
	putStrLn "1. Add a task"
	putStrLn "2. Mark a task as completed"
	putStrLn "3. Remove a task"
	putStrLn "4. Display the todo list"
	putStrLn "5. Quit"
	

	let todoList = emptyTodoList

	antiPenultimateMain todoList

--	pseudoMain todoList input


antiPenultimateMain todoList = do
	putStrLn "Welcome to the Todo application"
	putStrLn "Choose an option:"
	putStrLn "1. Add a task"
	putStrLn "2. Mark a task as completed"
	putStrLn "3. Remove a task"
	putStrLn "4. Display the todo list"
	putStrLn "5. Quit"
	
	input <- getLine

	pseudoMain todoList input
	
	

pseudoMain todoList input = do
	--input <- getLine
	case input of
	 "1" -> do
	   putStr "Enter the task to add:"
	   hFlush stdout
	   task <- getLine
	   let newTodoList = addTask todoList task
	   putStrLn "Task added"
	   antiPenultimateMain newTodoList
	 
	 "2" -> do
           putStr "Enter the task to mark as complete:"
	   hFlush stdout
	   task <- getLine
	   let newTodoList = completeTask todoList task
	   putStrLn "Task marked as complete"
	   antiPenultimateMain newTodoList
	   --pseudoMain newTodoList
	 
	 "3" -> do
	   putStr "What is the task should be removed:"
	   hFlush stdout
	   task <- getLine
	   let newTodoList = removeTask todoList task
	   putStrLn "Task marked as complete"
	   antiPenultimateMain newTodoList
	   --pseudoMain newTodoList

	 "4" -> do
	   putStrLn  "Displaying list ........ \n"
	   displayTodoList todoList
	   putStrLn "\n\n"
	   antiPenultimateMain todoList

	 "5" -> do
	   putStrLn "Bye for now !!"








	  








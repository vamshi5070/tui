{-# LANGUAGE OverloadedStrings #-}

module Main where

import Brick
import Brick.Widgets.Border (borderWithLabel, hBorder)
import Brick.Widgets.Center (hCenter)
import Brick.Widgets.Skylighting (attrMappingsForStyle, highlight)
import Control.Monad (void)
import Data.Maybe (fromJust)
import Data.Monoid ((<>))
import Path
import Data.Text (Text)
import Path.IO
import Data.Maybe
import qualified Data.Text.IO as T
import qualified Graphics.Vty as V
import qualified Skylighting.Core as S
import Skylighting.Syntax
import System.Environment (getArgs, getProgName)
import System.Exit (exitFailure)

ui :: Text -> Int -> [Widget ()]
ui programs styleIndex =
  [ progs]
  where
    help = hCenter $ txt "q/esc:quit   up/down:change theme"
    -- header = hCenter $ txt $ "Theme: " <> (fst $ styles !! styleIndex)
    progs = showProg programs
    showProg progSrc =
      (borderWithLabel (txt $ S.sName haskellSyntax) $ highlight haskellSyntax progSrc)


styles :: [(Text, S.Style)]
styles =
  [ ("breezeDark", S.breezeDark),
    ("espresso", S.espresso),
    ("kate", S.kate),
    ("pygments", S.pygments),
    ("tango", S.tango),
    ("haddock", S.haddock),
    ("monochrome", S.monochrome),
    ("zenburn", S.zenburn)
  ]

--handleEvent :: Int -> BrickEvent () e -> EventM () Int ()
--handleEvent (VtyEvent (V.EvKey V.KUp [])) = modify $ \i -> (i + 1) `mod` length styles
--handleEvent (VtyEvent (V.EvKey V.KDown [])) = modify $ \i -> (i - 1) `mod` length styles
handleEvent s (VtyEvent (V.EvKey V.KEsc [])) = halt s
handleEvent s (VtyEvent (V.EvKey (V.KChar 'q') [])) = halt s

--handleEvent _  _ = return

app :: Text -> App Int e ()
app programs =
  App
    { appDraw = ui programs,
      appAttrMap =
        const $
          attrMap V.defAttr $
            attrMappingsForStyle $ S.breezeDark,
      appHandleEvent = handleEvent,
      appChooseCursor = neverShowCursor,
      appStartEvent = return
    }

usage :: IO ()
usage = do
  pn <- getProgName
  putStrLn $ pn <> " <path to XML syntax definintion directory>"

main :: IO ()
main = do
  args <- getArgs
  fp <- case args of
    [p] -> return p
    -- [] -> return "rgv"
    _ -> usage >> exitFailure
  path <- resolveFile' fp
  maybeContents <- forgivingAbsence $ T.readFile (fromAbsFile path)
  let contents = fromMaybe "" maybeContents
  void $ defaultMain (app contents) 0

haskellSyntax = fromJust $ S.syntaxByName defaultSyntaxMap "haskell"

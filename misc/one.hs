removeOdd :: [a] -> [a]
removeOdd [] = []
removeOdd [x] = [x]
removeOdd (x:y:xs) = x: removeOdd xs

ans = until (\xs -> length xs == 1) removeOdd 

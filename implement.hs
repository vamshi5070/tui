{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

import Lens.Micro.Platform -- (^.) is get
import Data.Text (Text)
import Test.Hspec

main :: IO ()
main = return ()

data Address = Address
  { _street :: !Text
  , _city :: !Text
  }

makeLenses ''Address

data Person = Person
  {
    _name :: !Text
  , _address :: !Address
  , _age :: !Int
  }

makeLenses ''Person

hollywood :: Text
hollywood = "Hollywood "

aliceAddress :: Address
aliceAddress = Address 
  {
    _street = "Ambedkar street"
  , _city = "Hyderabad"
  }

alice :: Person
alice = Person
  {
    _name = "alice"
    ,_address = aliceAddress
    ,_age = 3
  }
  
whileshire :: Text
-- whileshire = "rgv"
whileshire = "Wilshire Blvd"

-- addressStreet :: Lens Address Address Text Text
-- addressStreet  = lens  street (\x y -> x {_street = y})

getStreet :: Person -> Text
getStreet p = p ^. address ^. street
-- aliceWhileshire :: Person
-- aliceWhileshire = 
birthday :: Person -> Person
-- birthday = undefined
birthday = over   age  (+1)

getAge :: Person -> Int
getAge p = p ^. age

aliceWhileshire :: Person
-- aliceWhileshire = undefined
aliceWhileshire = over (street . address ) (\_ -> whileshire ) alice 

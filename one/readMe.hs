import Brick (Widget,simpleMain,(<+>),str,withBorderStyle, joinBorders)
import Brick.Widgets.Center (center)
import Brick.Widgets.Border (borderWithLabel, vBorder)
import Brick.Widgets.Border.Style (unicode)

ui :: Widget ()
ui = joinBorders $  withBorderStyle unicode $ 
                        borderWithLabel (str "Hello!") $ 
                         center ( withBorderStyle unicode $ 
                                        borderWithLabel (str "Hello!") $ 
                                        center (str "Left") <+> vBorder <+> center (str "Right")
                         ) <+> vBorder <+> center (
                                withBorderStyle unicode $ 
                                        borderWithLabel (str "Hello!") $ 
                                        center (str "Left") <+> vBorder <+> center (str "Right")

                                                  )

-- block :: (Ord n) => Widget n
-- block =

main :: IO ()
main = simpleMain $ ui 

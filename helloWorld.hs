{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
module Main where

import Lens.Micro
import Lens.Micro.TH
import qualified Graphics.Vty as V

import qualified Brick.Main as M
import qualified Brick.Types as T
import Brick.Widgets.Core
	(
	  (<+>)
	, (<=>)
	, hLimit
	, vLimit
	, str
	)
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Edit as E
import qualified Brick.AttrMap as A
import qualified Brick.Focus as F
import qualified Brick.Util (on)

main :: IO ()
main = return ()

data Name = Edit1
	  | Edit2
	  deriving (Eq,Ord,Show)

data St =
    St { _focusRing :: F.FocusRing Name
    	,_edit1 :: E.Editor String Name
	,_edit2 :: E.Editor String Name
	}

makeLenses ''St

drawUI :: St -> [T.Widget Name]
drawUI st = [ui]
	where
          e1 = F.withFocusRing (st ^. focusRing) (E.renderEditor (str . unlines)) (st ^. edit1)
    	  e2 = F.withFocusRing (st ^. focusRing) (E.renderEditor (str . unlines)) (st ^. edit2)
          ui = C.center $
                  (str "Input 1 (unlimited): " <+> (hLimit 30 $ vLimit 5 e1)) <=>
                  str " " <=> 
                  (str "Input 2 (limited to 2 lines): " <+> (hLimit 30 e2)) <=>
                  str " " <=>
                  str "Press Tab to switch between editors, ESC to quit"

appEvent :: St -> T.BrickEvent Name e -> T.EventM Name (T.Next St)
appEvent st (T.VtyEvent (V.EvKey V.KEsc [])) = M.halt st
appEvent st (T.VtyEvent (V.EvKey (V.KChar '\t') [])) = M.continue $ st & focusRing %~ F.focusNext
appEvent st (T.VtyEvent (V.EvKey (V.KBackTab ) [])) = M.continue $ st & focusRing %~ F.focusPrev
appEvent st ev =
  M.continue =<< case F.focusGetCurrent (st ^. focusRing ) of
                   Just Edit1 -> T.handleEventLensed st edit1 E.handleEditorEvent ev
                   Just Edit2 -> T.handleEventLensed st edit2 E.handleEditorEvent ev
                   Nothing -> return st

module Logic (Game(..), Direction(..), Grid, printTile, initGame,
              insertRandomTileInGrid, stuckCheck, leftGrid, checkGridFull, scoreGrid)
              -- mainLogic, keepTrying)
        where

import System.Random  
import System.IO.Unsafe

type Tile = Maybe Int
type Row  = [Tile]
type Grid = [[Tile]]

-- checkFull = undefined

-- leftGrid = undefined

-- insertRandomTile = undefined

row :: Row 
row = [Just 4,Nothing,Just 2,Just 2]

grid :: Grid
grid = replicate 4 row

printTile :: Tile -> String
printTile (Just n) = show n
printTile Nothing = " "

printRow :: Row -> String
printRow = concatMap printTile

printGrid :: Grid -> String
printGrid = concatMap (\x -> printRow x <> "newline")

scoreRow :: Row -> Int -> Int -- tail call recursive
scoreRow [] n = n
scoreRow (Nothing:xs) n = scoreRow xs n
scoreRow ((Just x):xs) n 
        | x > n = scoreRow xs x
        | otherwise = scoreRow xs n

scoreRow' :: Row -> Int -> Int  -- using foldr
scoreRow' xs n = foldr helper n xs
        where helper Nothing acc = acc
              helper (Just x) acc 
                | x > acc = x
                | otherwise = acc

scoreGrid :: Grid -> Int -> Int
scoreGrid xss n = foldr helper n xss
        where helper xs acc = do
                let temp = scoreRow xs 0
                if temp > acc then temp else acc

-- mergeRow :: Row -> Row
-- mergeRow (Nothing:xs) = mergeRow xs
-- mergeRow (x:Nothing:xs) = mergeRow xs
--

stuckCheck :: Grid -> Bool
stuckCheck g = do
 case g of
     [[Just a,Just b, Just c, Just d],
      [Just e,Just f, Just g, Just h],
      [Just i,Just j, Just k, Just l],
      [Just m,Just n, Just o, Just p]] -> if (a == b || b == c || c == d ||
                                              e == f || f == g || g == h ||
                                              i == j || j == k || k == l ||
                                              m == n || n == o || o == p ||
                                              a == e || e == i || i == m ||
                                              b == f || f == j || j == n ||
                                              c == g || g == k || k == o ||
                                              d == h || h == l || l == p) then False else True
     _      -> False 

checkTileExists :: Tile -> Bool
checkTileExists Nothing = False
checkTileExists _ = True

checkRowFull :: Row -> Bool
checkRowFull = and . (map checkTileExists)

checkGridFull :: Grid -> Bool
checkGridFull = and . (map checkRowFull)

-- makeRandomTile :: Tile
-- makeRandomTile = do
--         let tempNum = unsafePerformIO getRand1To10 
--         if (tempNum :: Int) <= 6 then return 2 else return 4

getRand1To10 = getStdRandom $ randomR (1,10) 
getRand4To10 = getStdRandom $ randomR (4,10)
getRand1To2 :: IO Int
getRand1To2 = getStdRandom $ randomR (1,2) 

-- insertRandomTileInRow :: Row -> Row
insertRandomTileInRow (xs,True,cnt) = (xs,True,cnt)
insertRandomTileInRow (xs,False,cnt) = foldr helper ([],False,cnt) xs 
                where helper x (ys,True,cnt) = (x:ys,True,cnt)
                      helper (Just x) (ys,False,cnt) = ((Just x):ys,False,cnt)
                      helper Nothing (ys,False,cnt)
                        | cnt - 1 == 0 =  (makeRandomTile:ys,True,cnt-1)
                        -- | cnt - 1 <= 4 = if (unsafePerformIO getRand1To2 :: Int) == 1 then (Nothing:ys,False,cnt-1) else (makeRandomTile:ys,True,cnt-1)
                        | (unsafePerformIO getRand4To10 :: Int) <= 8 = (Nothing:ys,False,cnt-1) 
                        | (unsafePerformIO getRand1To10 :: Int) <= 6 =  (makeRandomTile:ys,True,cnt-1)
                        | otherwise = (Nothing:ys,False,cnt-1)
                      makeRandomTile = if (unsafePerformIO getRand1To10) <= 6 then return 2 else return 4

-- insertRandomTileInGrid :: Grid -> Grid
insertRandomTileInGrid xss = (\(yss,_,_) -> yss) $ insertRandomWithTruth xss
  
-- insertRandomWithTruth :: Grid -> (Grid,Bool)
insertRandomWithTruth xss = foldr helper ([],False,cnt) xss
        where helper ys (yss,True,cnt) = (ys:yss,True,cnt)
              helper ys (yss,False,cnt) = let (ys',resBool,resCnt) = insertRandomTileInRow (ys,False,cnt)  
                  in
                (ys':yss,resBool,resCnt)
              cnt = count Nothing xss

-- uses count to count no . of empty tiles (Nothing)  every time it is required to insert a randomTile
count :: (Eq a) => a -> [[a]] -> Int
count key xss = length  [x |  xs <- xss,x <- xs , x == key ]

emptyGrid = replicate 4 emptyRow 
emptyRow = replicate 4 Nothing

makePairs :: [a] -> [(a,a)]
makePairs xs = zip xs (tail xs) 

xs = [1..4]
--
-- mergeRow [] = []
-- mergeRow (xy:xys) = (mergeHead xy) <> mergeTail xys
--
-- mergeHead (Just x,Just y) 
--         | x /= y = [Just x,Just y]
--         | otherwise = [Just (2*x)] 
-- mergeHead (Nothing,x) = [x]
-- mergeHead (x,Nothing) = [x]
--
-- mergeTail xys =  foldr helper [] xys
--         where helper (Just x,Just y) acc 
--                   | x /= y = (Just x):acc
--                   | otherwise = Just (2*x):acc
--               helper (Nothing,x) acc  = acc
--               helper (x,Nothing) acc = x:acc
--
-- example: mergeRow [Just 4, Nothing, Just 2, Just 2] -> [Just 4,Just 4]
mergeRow :: Row -> Row 
mergeRow (Nothing:xs)= mergeRow xs
mergeRow (x:Nothing:xs) = mergeRow (x:xs)
mergeRow ((Just x):(Just y):xs)
        | x == y =(Just $ x * 2) : (mergeRow xs) 
        | otherwise = Just x :(mergeRow ((Just y):xs))
mergeRow xs = xs
  -- Nothing:xs -> mergeRow xs
  -- x:Nothing:xs -> mergeRow (x:xs)
  -- (Just x):(Just y):xs -> if else   xs -> xs

-- example: leftRow  [Just 4,Just 4] -> [Just 4,Just 4,Nothing,Nothing]
leftRow :: Row -> Row   -- left swipe 
leftRow xs = xs' <> (replicate (4-x) Nothing)
        where x = length xs'
              xs' = mergeRow xs

leftGrid :: Grid -> Grid
leftGrid = map leftRow

decision =  tempNum <= 6 
        where tempNum = unsafePerformIO getRand1To10

main = print $ insertRandomTileInGrid emptyGrid

-- game Definitions 
-- Game state:
data Game = Game 
        { _grid :: Grid
        , _score :: Int
        , _done :: Bool
        } deriving (Eq,Show)

data Direction
  = Up
  | Down
  | Left
  | Right
  deriving (Eq, Show)

initialGrid =  [[Just 2, Just 2, Nothing, Nothing],
                    [Nothing, Nothing, Nothing, Nothing],
                    [Nothing, Nothing, Nothing, Nothing],
                    [Nothing, Nothing, Nothing, Nothing]]

-- add options for bot later
initGame :: IO Game
initGame = do
  return $
          Game { _grid =  initialGrid
         , _score = 0
        , _done = False
        }

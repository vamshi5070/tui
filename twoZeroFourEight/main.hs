import Data.Maybe
import Data.List

import Logic
  (Game(..), Direction(..), Grid, printTile, initGame, insertRandomTileInGrid,
  stuckCheck, leftGrid, checkGridFull, scoreGrid)

import Control.Concurrent (threadDelay, forkIO)

import Control.Monad.IO.Class (liftIO)
import Control.Monad (forever, void)

import Brick
import Brick.BChan (newBChan, writeBChan) 
import qualified Brick.Widgets.Border as B
import qualified Brick.Widgets.Border.Style as BS
import qualified Brick.Widgets.Center as C
import qualified Brick.Util as U
import qualified Graphics.Vty as V
--
type Name = ()
data Tick = Tick

humanPlayer :: IO ()
humanPlayer = do
  chan <- newBChan 10
  forkIO $ forever $ do
    writeBChan chan Tick
    threadDelay 100000 -- decides how fast your game moves
  g <- initGame
  myVty <- (V.mkVty V.defaultConfig) 
  void $ customMain myVty (V.mkVty V.defaultConfig) (Just chan) app g

-- color attributes:
gameOverAttr, blueBg, brblBg, cyanBg, bcyanBg, yellowBg, byellowBg, greenBg, bgreenBg,  whiteBg  :: AttrName
gameOverAttr = attrName "gameOver"
blueBg = attrName "blueBg"
brblBg = attrName "brblBg"
cyanBg = attrName "cyanBg"
bcyanBg = attrName "bcyanBg"
magBg = attrName "magBg"
bmagBg = attrName "bmagBg"
yellowBg = attrName "yellowBg"
byellowBg = attrName "byellowBg"
greenBg = attrName "greenBg"
bgreenBg = attrName "bgreenBg"
whiteBg = attrName "whiteBg"

theMap :: AttrMap
theMap = attrMap V.defAttr
  [
  (gameOverAttr, fg V.red `V.withStyle` V.bold),
  (blueBg, U.fg V.blue),
  (brblBg, U.fg V.brightBlue),
  (cyanBg, U.fg V.cyan),
  (bcyanBg, U.fg V.brightCyan),
  (yellowBg, U.fg V.yellow),
  (byellowBg, U.fg V.brightYellow),
  (magBg, U.fg V.magenta),
  (bmagBg, U.fg V.brightMagenta),
  (greenBg, U.fg V.green),
  (bgreenBg, U.fg V.brightGreen),
  (whiteBg, U.bg V.white)
  ]

-- define App
app :: App Game Tick Name
app = App { appDraw = drawUI
          , appChooseCursor = neverShowCursor
          , appHandleEvent = handleEvent
          , appStartEvent = return
          , appAttrMap = const theMap
          }

drawInfo :: Widget Name
drawInfo = withBorderStyle BS.unicodeBold
  $ hLimit 20
  $ B.borderWithLabel (str "Commands")
  $ vBox $ map (uncurry3 drawKey)
  $ [ ("Left", "h",blueBg)
    , ("Right", "l",brblBg)
    , ("Down", "j",cyanBg)
    , ("Up", "k",bcyanBg)
    , ("Restart", "r",magBg)
    , ("Quit", "q or esc",bgreenBg)
    ]
  where
    drawKey act key color = (padRight Max $ padLeft (Pad 1) $ withAttr color (str act))
                      <+> (padLeft Max $ padRight (Pad 1) $ withAttr color (str key))

uncurry3 :: (a -> b -> c -> d) -> (a,b,c) -> d
uncurry3 f (x,y,z) =  f x y z

-- Drawing
drawUI :: Game -> [Widget Name]
drawUI g =
  [ C.center $  drawGrid g  <+> vBox [padLeft (Pad 4) drawInfo, padTop (Pad 4) $ drawGameOver (_done g)]] 
-- <+> padLeft (Pad 4) (drawStats g) 
drawStats :: Game -> Widget Name
drawStats g = hLimit 21
  $ vBox [ drawScore (_score g) , padTop (Pad 2) $ drawGameOver (_done g)]

drawScore :: Int -> Widget Name
drawScore n = withBorderStyle BS.unicodeBold
  $ withAttr gameOverAttr
  $ B.borderWithLabel (str "Score")
  $ C.hCenter
  $ padAll 1
  $ str $ show n

drawGrid :: Game -> Widget Name
drawGrid g = withBorderStyle BS.unicodeBold
  $ B.borderWithLabel (withAttr magBg $ str $ show (_score g)) -- str "2048")
  $ vBox rows
  where
    rows = [hBox $ tilesInRow r | r <- (_grid g)]
    tilesInRow row = [hLimit 9 $ withBorderStyle BS.unicodeBold $ B.border $ C.hCenter $ padAll 1 $ colorTile $ printTile tile | tile <- row]

colorTile val = case val of
  "2" -> withAttr blueBg $ str val
  "4" -> withAttr brblBg $ str val
  "8" -> withAttr cyanBg $ str val
  "16" -> withAttr bcyanBg $ str val
  "32" -> withAttr magBg $ str val
  "64" -> withAttr bmagBg $ str val
  "128" -> withAttr yellowBg $ str val
  "256" -> withAttr byellowBg $ str val
  "512" -> withAttr greenBg $ str val
  "1024" -> withAttr bgreenBg $ str val
  "2048" -> withAttr whiteBg $ str val
  _ -> str val

drawGameOver :: Bool -> Widget Name
drawGameOver done =
  if done
    then withAttr gameOverAttr $ C.hCenter $ str "GAME OVER"
    else emptyWidget
-- drawInfo :: Widget ()
-- drawInfo = withBorderStyle BS.unicode
        -- $ C.hCenter
        -- $ hLimit 80
        -- $ vLimit 400
        -- $ B.borderWithLabel (str "First hit enter. Then type which player you are.")
        -- $ vBox $ map (uncurry drawKey)
        -- $ [ ("h", "Human Player (YOU!)")
        --   -- , ("u", "Up Bot (Always moves up)")
          -- , ("r", "Random Bot")
          -- , ("m", "monte carlo bot")
         -- ]
        -- where 
                -- drawKey act key = (padRight Max $ padLeft (Pad 1) $ str act)
                        -- <+> (padLeft Max $ padRight (Pad 1) $ str key)

processLine :: String -> IO ()
processLine "h" = humanPlayer
-- (str "rgv")
-- humanPlayer = undefined
experiment = [(str "rgv"),(str "rgv"),(str "rgv"),(str "rgv")]

-- main = do
--   simpleMain drawInfo
--   c <- getLine
--   processLine c
--
isGameOver :: Game -> Bool
isGameOver g = (checkGridFull (_grid g)) && (stuckCheck (_grid g))

step :: Game -> Game
step g 
  | isGameOver g = Game {_grid = _grid g, _score = _score g, _done = True}
  | otherwise = g

handle :: Logic.Direction -> Grid -> Grid
handle Logic.Left xss = leftGrid xss
handle Logic.Up xss = transpose (leftGrid (transpose xss))
handle Logic.Right xss = map reverse (leftGrid (map reverse xss))
handle Logic.Down xss =  transpose $ map reverse (leftGrid (map reverse (transpose xss)))
 
-- handle Logic.Down xss = transpose (leftGrid (transpose xss))
move :: Logic.Direction -> Game -> Game
move dir game =
        Game { _grid = newGrid
             , _score = (scoreGrid newGrid 0)  
             , _done =  (checkGridFull newGrid && stuckCheck newGrid)
             }
                     where  newGrid = if changedGrid == grid then grid else insertRandomTileInGrid changedGrid
                            changedGrid = handle dir grid
                            grid = _grid game
                            
handleEvent :: Game -> BrickEvent Name Tick -> EventM Name (Next Game)
handleEvent g (AppEvent Tick)                       = continue $ step g
handleEvent g (VtyEvent (V.EvKey (V.KChar 'k') []))         = continue $ move Logic.Up g
handleEvent g (VtyEvent (V.EvKey (V.KChar 'j') []))       = continue $ move Logic.Down g
handleEvent g (VtyEvent (V.EvKey (V.KChar 'l') []))      = continue $ move Logic.Right g
handleEvent g (VtyEvent (V.EvKey (V.KChar 'h') []))       = continue $ move Logic.Left g
handleEvent g (VtyEvent (V.EvKey (V.KChar 'r') [])) = liftIO (initGame) >>= continue
handleEvent g (VtyEvent (V.EvKey (V.KChar 'q') [])) = halt g
handleEvent g (VtyEvent (V.EvKey V.KEsc []))        = halt g
handleEvent g _                                     = continue g

main :: IO ()
main =  humanPlayer
